// import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import AppNavBar from './Components/AppNavbar';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
// import Banner from './Components/Banner';
// import Highlights from './Components/Highlights';
import Register from './pages/Registration';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import './App.css';
import { UserProvider } from './UserContext';


function App() {

  // State hook for the user state that's defined here for a global scope
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  });

  // This function is for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  // 
  useEffect(() => {
    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }

      else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar/>
        <Container>
          <Routes>
            <Route path="/" element= {<Home/>}/>
            <Route path="/courses" element= {<Courses/>}/>
            <Route path="/courses/:courseId" element= {<CourseView/>}/>
            <Route path="/login" element= {<Login/>}/>
            <Route path="/logout" element= {<Logout/>}/>
            <Route path="/register" element= {<Register/>}/>
            <Route path="*" element= {<NotFound/>}></Route>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;


// Notes:
/*

  If returning a multiple component, you need to insert it in a fragment component.
    - Import Fragment
      1. import { Fragment } from 'react';
      2. <Fragment> children components here </Fragment> 
    - Fragment Shorthand
      1. <> children components here </>

*/