// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
// import Container from 'react-bootstrap/Container';
import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar() {

    // State to store the user information stored in the login page
    // const [user, setUser] = useState(localStorage.getItem("email"));
    /*
        Syntax:
            localStorage.getItem("propertyName")
        getItem() - method that returns value of a specified object item
    */

    const { user } = useContext(UserContext);

    console.log(user);

    return (
        <Navbar bg="light" expand="lg">
        <Container>
            <Navbar.Brand as = {Link} to="/">Batch 189</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
                <Nav.Link as = {Link} to="/">Home</Nav.Link>
                <Nav.Link as = {Link} to="/courses">Courses</Nav.Link>
                {

                    (user.id !== null)
                        ?   <Nav.Link as = {Link} to="/logout">Log Out</Nav.Link>
                        :   <>
                                <Nav.Link as = {Link} to="/login">Log in</Nav.Link>
                                <Nav.Link as = {Link} to="/register">Register</Nav.Link>
                            </>
                }
            </Nav>
            </Navbar.Collapse>
        </Container>
        </Navbar>
    );

};