import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

    // console.log(props);
    // console.log(typeof props)
    // console.log(props.courseProp.name)

    /*

        - Use the state hook for this component to be able to store its state
        - States are used keep track of information related to individual components

        Syntax:
            const [getter, setter] = useState(initialGetterValue)

    */

    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(30);


    // function enroll() {

    //     // Solution 2
    //     // if (count === 29) {
    //     //     event.currentTarget.disabled = true;
    //     // }

    //     // if (count === 30 && slots === 0) {
    //     //     alert ('No slots left')
    //     //     return;
    //     // }

    //     setCount(count + 1);
    //     setSeats(seats - 1);
    
    // }


    // // [useEffect()]
    // /*

    //     Syntax:
    //         useEffect(()=>{}, [state]) 
    //             - this function will run everytime, it will always check the value of the state
            
    //         useEffect(()=>{})
    //             -this function will run everytime, but doesn't specifically watch the value of state

    //         useEffect(() =>, [])
    //             - the empty array will indicate that the useEffect will only run once the components is rendered

    // */
    // useEffect(() => {

    //     if (seats === 0) {
    //         alert('No more seats available.')
    //     }

    // }, [seats]);


    // Deconstruct the course properties into their own variables
    const { name, description, price, _id } = courseProp;
    console.log(courseProp);


    
    return (
        <Card className="p-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PHP {price}</Card.Text>
                <Button
                    variant="primary"
                    id="btn-enroll"
                    as={ Link }
                    to={`/courses/${_id}`}>
                    Details
                </Button>
            </Card.Body>
        </Card>
    )
}

/*

    <Link className="btn btn-primary" to="/courseView">Details</Link>

*/