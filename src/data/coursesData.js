const coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla est sapien. In hac habitasse platea dictumst. Quisque tristique malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla est sapien. In hac habitasse platea dictumst. Quisque tristique malesuada.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python-Django",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla est sapien. In hac habitasse platea dictumst. Quisque tristique malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla est sapien. In hac habitasse platea dictumst. Quisque tristique malesuada.",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java-Spinrtboot",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla est sapien. In hac habitasse platea dictumst. Quisque tristique malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla est sapien. In hac habitasse platea dictumst. Quisque tristique malesuada.",
        price: 55000,
        onOffer: true
    }
];

export default coursesData;