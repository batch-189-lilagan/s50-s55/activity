import Highlights from "../Components/Highlights";
import Banner from "../Components/Banner";
// import CourseCard from "../Components/CourseCard";


export default function Home() {

    return (

        <>
            <Banner />
            <Highlights/>
            {/* <CourseCard /> */}
        </>

    );

};